import 'package:flutter/material.dart';

import 'custom_appbar.dart';

class BaseSingleViewPage extends Scaffold {
  BaseSingleViewPage(
      {Key? key,
      Key? scrollViewKey,
      bool extendBodyBehindAppBar = false,
      PreferredSizeWidget? appBar,
      Color? backgroundColor,
      required Widget body,
      WillPopCallback? onWillPop,
      Brightness brightness = Brightness.light,
      Widget? drawer,
      EdgeInsetsGeometry? padding,
      bool? resizeToAvoidBottomInset,
      Widget? bottomNavigationBar,
      Widget? floatingActionButton,
      FloatingActionButtonLocation? floatingActionButtonLocation})
      : super(
          key: key,
          extendBodyBehindAppBar: extendBodyBehindAppBar,
          appBar: appBar,
          backgroundColor: backgroundColor,
          drawer: drawer,
          body: SingleChildScrollView(
            key: scrollViewKey,
            padding: padding,
            child: body,
          ),
          resizeToAvoidBottomInset: resizeToAvoidBottomInset,
          bottomNavigationBar: bottomNavigationBar,
          floatingActionButton: floatingActionButton,
          floatingActionButtonLocation: floatingActionButtonLocation,
        );
}
