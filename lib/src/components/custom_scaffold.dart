import 'package:flutter/material.dart';

class BaseScaffold extends Scaffold {
  const BaseScaffold(
      {Key? key,
      String? title,
      bool extendBodyBehindAppBar = false,
      PreferredSizeWidget? appBar,
      Color? backgroundColor,
      Widget? drawer,
      required Widget body,
      bool? resizeToAvoidBottomInset,
      Widget? floatingActionButton,
      Widget? bottomNavigationBar,
      FloatingActionButtonLocation? floatingActionButtonLocation})
      : super(
          key: key,
          extendBodyBehindAppBar: extendBodyBehindAppBar,
          appBar: appBar,
          backgroundColor: backgroundColor,
          drawer: drawer,
          body: body,
          resizeToAvoidBottomInset:resizeToAvoidBottomInset,
          floatingActionButton: floatingActionButton,
					bottomNavigationBar: bottomNavigationBar,
          floatingActionButtonLocation: floatingActionButtonLocation,
        );
}
